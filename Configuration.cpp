#include "Configuration.h"

#define MAXCARDS 53

std::vector<std::string > const Configuration::_cards = []()
{
	std::vector<std::string> cards;

	for (int i = 1; i < MAXCARDS; i++)
		cards.push_back(std::string("Assets/Pictures/Cards/card (" + std::to_string(i) + ").svg"));

	return cards;
}();

std::string const Configuration::_cardBack = "Assets/Pictures/purple_back.jpg";
std::string const Configuration::_background = "Assets/Pictures/background.jpg";
std::string const Configuration::_brick = "Assets/Pictures/brick.png";
std::string const Configuration::_table = "Assets/Pictures/table.png";
std::string const Configuration::_leftArrow = "Assets/Pictures/left_arrow.png";
std::string const Configuration::_rightArrow = "Assets/Pictures/right_arrow.png";
std::string const Configuration::_startGame = "Assets/Pictures/start_game.png";
std::string const Configuration::_back = "Assets/Pictures/back_button.png";
std::string const Configuration::_gameMusic = "Assets/Sounds/game_music.mp3";
std::string const Configuration::_cardTone = "Assets/Sounds/high.wav";
std::string const Configuration::_font = "Assets/Fonts/arial.ttf";

const std::string& Configuration::GetCardPath(int i)
{
	return _cards[i];
}

const std::string& Configuration::GetCardBackPath()
{
	return _cardBack;
}

const std::string& Configuration::GetBackgroundPath()
{
	return _background;
}

const std::string& Configuration::GetTablePath()
{
	return _table;
}

const std::string& Configuration::GetLeftArrowPath()
{
	return _leftArrow;
}

const std::string& Configuration::GetRightArrowPath()
{
	return _rightArrow;
}

const std::string& Configuration::GetStartGameButtonPath()
{
	return _startGame;
}

const std::string& Configuration::GetBackButtonPath()
{
	return _back;
}

const std::string& Configuration::GetGameMusicPath()
{
	return _gameMusic;
}

const std::string& Configuration::GetCardTonePath()
{
	return _cardTone;
}

const std::string& Configuration::GetFontPath()
{
	return _font;
}

const std::string& Configuration::GetBrickPath()
{
	return _brick;
}

  