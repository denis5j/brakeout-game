#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <string>
#include <vector>

class Configuration
{
public:

	static const std::string& GetCardPath(int i);
	static const std::string& GetCardBackPath();
	static const std::string& GetBackgroundPath();
	static const std::string& GetTablePath();
	static const std::string& GetLeftArrowPath();
	static const std::string& GetRightArrowPath();
	static const std::string& GetStartGameButtonPath();
	static const std::string& GetBackButtonPath();
	static const std::string& GetGameMusicPath();
	static const std::string& GetCardTonePath();
	static const std::string& GetFontPath();
	static const std::string& GetBrickPath();

protected:
	static std::vector<std::string> const _cards;
	static std::string const _cardBack;
	static std::string const _background;
	static std::string const _table;
	static std::string const _leftArrow;
	static std::string const _rightArrow;
	static std::string const _startGame;
	static std::string const _back;
	static std::string const _gameMusic;
	static std::string const _cardTone;
	static std::string const _font;
	static std::string const _brick;
};

#endif