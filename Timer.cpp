#include "Timer.h"
#include <SDL.h>

Timer::Timer()
{
	SDL_Init(SDL_INIT_TIMER);
	Reset();
}

Timer::~Timer()
{ 
}

int Timer::GetMilisecondsDelta() const
{
	return SDL_GetTicks() - m_milliSecondTicks;
}

void Timer::Reset()
{
	m_milliSecondTicks = SDL_GetTicks();
}
