#ifndef DRAWABLE_H
#define DRAWABLE_H

#include <string>
#include <vector>
#include "Texture/Texture.h"

#include <glm/vec2.hpp>
#include <glm/mat2x2.hpp>
#include <glm/gtx/string_cast.hpp>

struct SDL_Texture;

class Drawable
{
public:
	Drawable(glm::vec2 position, glm::vec2 size, double angle = 0.0);
	virtual ~Drawable();

	SDL_Texture* GetTexture() const;
	void SetImageTexture(std::string path);
	void SetTextTexture(std::string text, int fontSize, int r, int g, int b);

	bool IsInrange(float x, float y) const;
	
	void SetPosition(glm::vec2 position);
	glm::vec2 GetPosition() const;

	void SetSize(glm::vec2& size);
	glm::vec2 GetSize() const;

	void SetRotation(glm::vec2& rotation);
	glm::vec2 GetRotation() const;

	void SetAngle(double angle, int x = 0, int y = 0);
	double GetAngle()const;

	glm::vec2 GetCenter();

	static std::vector<Drawable const*> _activeDrawables;
	
protected:
	glm::vec2 _drawingPosition;
	glm::vec2 _centerPosition;
	glm::vec2 _drawingRotation;
	glm::vec2 _drawingSize;

	double _angle;
	
	std::string _path;
	std::unique_ptr<Texture> _texture;
};

#endif