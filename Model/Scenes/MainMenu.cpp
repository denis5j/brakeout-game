#include "MainMenu.h"
#include "../../Configuration.h"

MainMenu::MainMenu() : Updatable()
{
	//_guiTexts.push_back(std::make_unique<Text>(600, 180, "Number of players : ", 60));
	//_guiTexts.push_back(std::make_unique<Text>(600, 420, "Number of cards in row : ", 60));
	//_guiTexts.push_back(std::make_unique<Text>(600, 660, "Number of cards in col : ", 60));

	////_numberOfPlayersText = std::make_unique<Text>(600, _guiTexts[0]->GetY() + _guiTexts[0]->GetSizeY() + 100, std::to_string(_numberOfPlayers), 60);
	////_changeNumberOfPlayersButtons.first = std::make_unique<Button>(0, _guiTexts[0]->GetY() + _guiTexts[0]->GetSizeY() + 50, 100, 100, Configuration::GetLeftArrowPath(), false);
	////_changeNumberOfPlayersButtons.second = std::make_unique<Button>(0, _changeNumberOfPlayersButtons.first->GetY(), 100, 100, Configuration::GetRightArrowPath());

	////_widthText = std::make_unique<Text>(600, _guiTexts[1]->GetY() + _guiTexts[1]->GetSizeY() + 100, std::to_string(_cardArrayWidth), 60);
	////_changeWidthButtons.first = std::make_unique<Button>(0, _guiTexts[1]->GetY() + _guiTexts[1]->GetSizeY() + 50, 100, 100, Configuration::GetLeftArrowPath(), false);
	////_changeWidthButtons.second = std::make_unique<Button>(0, _changeWidthButtons.first->GetY(), 100, 100, Configuration::GetRightArrowPath());

	////_heightText = std::make_unique<Text>(600, _guiTexts[2]->GetY() + _guiTexts[2]->GetSizeY() + 100, std::to_string(_cardArrayHeight), 60);
	////_changeHeightButtons.first = std::make_unique<Button>(0, _guiTexts[2]->GetY() + _guiTexts[2]->GetSizeY() + 50, 100, 100, Configuration::GetLeftArrowPath(), false);
	////_changeHeightButtons.second = std::make_unique<Button>(0, _changeHeightButtons.first->GetY(), 100, 100, Configuration::GetRightArrowPath());

	//_guiTexts.push_back(std::make_unique<Text>(600, 50, "Memory game", 80));

	_startGame = std::make_unique<Button>(glm::vec2(600 - 150, 900), glm::vec2(300, 100), Configuration::GetStartGameButtonPath());
}

void MainMenu::OnUpdate(float deltaTime)
{
	_numberOfPlayersText->SetText(std::to_string(_numberOfPlayers));
	_widthText->SetText(std::to_string(_cardArrayWidth));
	_heightText->SetText(std::to_string(_cardArrayHeight));/*
	_changeNumberOfPlayersButtons.first->SetX(550 - _numberOfPlayersText->GetSizeX() / 2.0f - 30);
	_changeNumberOfPlayersButtons.second->SetX(550 + _numberOfPlayersText->GetSizeX() / 2.0f + 30);
	_changeWidthButtons.first->SetX(550 - _widthText->GetSizeX() / 2.0f - 30);
	_changeWidthButtons.second->SetX(550 + _widthText->GetSizeX() / 2.0f + 30);
	_changeHeightButtons.first->SetX(550 - _heightText->GetSizeX() / 2.0f - 30);
	_changeHeightButtons.second->SetX(550 + _heightText->GetSizeX() / 2.0f + 30);*/
}

Button* MainMenu::GetStartButton() const
{
	return _startGame.get();
}

std::pair<Button*, Button*> MainMenu::GetNumberOfPlayersButtons() const
{
	return std::pair<Button*, Button*>(_changeNumberOfPlayersButtons.first.get(), _changeNumberOfPlayersButtons.second.get());
}

std::pair<Button*, Button*> MainMenu::GetWidthButtons() const
{
	return std::pair<Button*, Button*>(_changeWidthButtons.first.get(), _changeWidthButtons.second.get());
}

std::pair<Button*, Button*> MainMenu::GetHightButtons() const
{
	return std::pair<Button*, Button*>(_changeHeightButtons.first.get(), _changeHeightButtons.second.get());
}