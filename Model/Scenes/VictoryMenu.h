#ifndef VICTORY_MENU_H
#define VICTORY_MENU_H

#include <vector>
#include <memory>

#include "../Elements/Button.h"
#include "../Elements/Text.h"
#include "../Elements/Player.h"

class VictoryMenu
{
public:
	VictoryMenu(std::vector<Player*> players);

	void SortPlayersByPoints(std::vector<Player*> &players) const;

	Button* GetBackButton() const;
protected:

	void CreateWinnersAndRangList(std::vector<Player*> &players);

	std::unique_ptr<Text> _victory;
	std::unique_ptr<Text> _top20;
	std::unique_ptr<Text> _winers;
	std::vector<std::unique_ptr<Text>> _victoryPlayers;
	std::vector<std::unique_ptr<Text>> _playerPoints;

	std::unique_ptr<Button> _backButton;
};

#endif