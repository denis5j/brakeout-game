#ifndef MAIN_MENU_H
#define MAIN_MENU_H

#include <vector>
#include <memory>

#include "../Elements/Button.h"
#include "../Elements/Background.h"
#include "../Elements/Text.h"
#include "../Updatable.h"

class MainMenu : public Updatable
{
public:
	MainMenu();

	void OnUpdate(float deltaTime);

	Button* GetStartButton() const;

	std::pair<Button*, Button*> GetNumberOfPlayersButtons() const;
	std::pair<Button*, Button*> GetWidthButtons() const;
	std::pair<Button*, Button*> GetHightButtons() const;

	int _numberOfPlayers = 1;
	int _cardArrayHeight = 2;
	int _cardArrayWidth = 2;

protected:

	std::unique_ptr<Text> _numberOfPlayersText;
	std::unique_ptr<Text> _widthText;
	std::unique_ptr<Text> _heightText;
	std::vector<std::unique_ptr<Text>> _guiTexts;

	std::pair<std::unique_ptr<Button>, std::unique_ptr<Button>> _changeNumberOfPlayersButtons;
	std::pair<std::unique_ptr<Button>, std::unique_ptr<Button>> _changeWidthButtons;
	std::pair<std::unique_ptr<Button>, std::unique_ptr<Button>> _changeHeightButtons;
	std::unique_ptr<Button> _startGame;
};

#endif