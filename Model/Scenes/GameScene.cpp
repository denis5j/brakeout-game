#include "GameScene.h"
#include "../../Configuration.h"
#include "../../View/Window.h"
#include <algorithm>
#include <iterator>

GameScene::GameScene(std::vector<Player*> players, std::shared_ptr<XMLParser> xmlParser) :_players(players)
{
	_xmlParser = xmlParser;

	CreateBall();
	CreateWalls();
	LoadBricks();
	CrateGameText(players);
}

GameScene::~GameScene()
{
}

void GameScene::CrateGameText(std::vector<Player*> players)
{
	auto numberOfPlayers = players.size();

	_currentPlayerPointsText = std::make_unique<Text>(glm::vec2(500, 500), players[0]->GetName() + " points : 0", 25, true);
}

void GameScene::SetCurrentPlayerPointsText(Player* player)
{
	_currentPlayerPointsText->SetText(player->GetName() + " points : " + std::to_string(player->GetPoints()));
}

void GameScene::CreateBall()
{
	_ball = std::make_unique<Ball>(glm::vec2(500, 700), glm::vec2(10, 10));
}

void GameScene::CreateWalls()
{
	_walls.push_back(std::make_unique<Wall>(glm::vec2(1100, 0), glm::vec2(100, 1000), glm::vec2(0, 0))); //desni
	_walls.push_back(std::make_unique<Wall>(glm::vec2(), glm::vec2(1200, 100), glm::vec2(0, 0))); //gore
	_walls.push_back(std::make_unique<Wall>(glm::vec2(), glm::vec2(100, 1000), glm::vec2(0, 0))); //lijevo
}

void GameScene::CreateBricks(int rowCount, int colCount, int rowSpacing, int colSpacing)
{
	auto bricksMap = _xmlParser->GetBricks();
	auto brickInfo = _xmlParser->GetBrickTypes();
	auto brickSize = CalaculateBrickSize(rowCount, colCount, rowSpacing, colSpacing);

	for (int i = 0; i < rowCount; i++)
	{		 
		for (int j = 0; j < colCount; j++)
		{
			std::string id;
			id.push_back(bricksMap[(j + i * colCount) * 2]);

			if (id == "_")
				continue;

			auto textureString = brickInfo[id]["Texture"];
			auto hitPointsString = brickInfo[id]["HitPoints"];
			auto hitSoundString = brickInfo[id]["HitSound"];
			auto breakScoreString = brickInfo[id]["BreakScore"];
			auto breakSoundString = brickInfo[id]["BreakSound"];

			int hitPoints = hitPointsString != "Infinite" ? std::atoi(hitPointsString.c_str()) : 1;
			int breakScore = std::atoi(breakScoreString.c_str());

			_bricks.push_back(std::make_unique<Brick>(id[0], textureString, hitPoints, hitSoundString, breakScore, breakSoundString, glm::vec2(_walls[2]->GetSize().x + j * (colSpacing + brickSize.x), _walls[1]->GetSize().y + i * (rowSpacing + brickSize.y)), brickSize, hitPointsString == "Infinite"));
		}
	}
}

void GameScene::LoadBricks()
{
	int rowCount = std::stoi(_xmlParser->GetLevelData("RowCount"));
	int colCount = std::stoi(_xmlParser->GetLevelData("ColumnCount"));
	int rowSpacing = std::stoi(_xmlParser->GetLevelData("RowSpacing"));
	int colSpacing = std::stoi(_xmlParser->GetLevelData("ColumnSpacing"));

	CreateBricks(rowCount, colCount, rowSpacing, colSpacing);
}

glm::vec2 GameScene::CalaculateBrickSize(int rowCount, int colCount, int rowSpacing, int colSpacing)
{
	glm::vec2 space = glm::vec2(Window::GetWidth(), Window::GetHeight() / 2.0f);

	space -= glm::vec2(_walls[0]->GetSize().x, 0);
	space -= glm::vec2(0, _walls[1]->GetSize().y);
	space -= glm::vec2(_walls[2]->GetSize().x, 0);
	space -= glm::vec2((colCount - 1) * colSpacing, (rowCount - 1) * rowSpacing);

	auto brickSize = space / glm::vec2(colCount, rowCount);

	return brickSize;
}

void GameScene::UpdateBricks()
{
	std::vector<std::unique_ptr<Brick>> _nisuObrisani;

	for (auto& brick : _bricks)
	{
		if (brick->GetHitPoints() > 0)
			_nisuObrisani.push_back(std::move(brick));
		else
			_players[0]->GivePoints(brick->GetBreakScore());
	}

	_bricks = std::move(_nisuObrisani);
}
