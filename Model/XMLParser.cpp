#include "XMLParser.h"

#include "../tinyxml.h"

XMLParser::XMLParser()
{
    _tags.push_back("Id");
    _tags.push_back("Texture");
    _tags.push_back("HitPoints");
    _tags.push_back("HitSound");
    _tags.push_back("BreakSound");
    _tags.push_back("BreakScore");

	file = std::make_unique<TiXmlDocument>();
}

XMLParser::~XMLParser()
{
}
void XMLParser::LoadFile(std::string path)
{
	file->LoadFile(path.c_str());
}

std::string XMLParser::GetLevelData(std::string dataName)
{
	return std::string(file->FirstChildElement("Level")->Attribute(dataName.c_str()));
}
std::map<std::string,std::map<std::string,std::string>> XMLParser::GetBrickTypes()
{
    std::map<std::string, std::map<std::string, std::string>> bricks;

    TiXmlHandle hDoc(file.get());
    TiXmlElement* pRoot, * pParm;
    pRoot = file->FirstChildElement("Level");
    pRoot = pRoot->FirstChildElement("BrickTypes");
    if (pRoot)
    {
        pParm = pRoot->FirstChildElement("BrickType");

        while (pParm)
        {
            std::map<std::string, std::string> brick;

            for (auto tag : _tags)
            {
                auto data = std::string(pParm->Attribute(tag.c_str()) == 0 ? "" : pParm->Attribute(tag.c_str()));
                brick[tag] = data;
            }

            bricks[std::string(pParm->Attribute(_tags[0].c_str()))] = brick;

            pParm = pParm->NextSiblingElement("BrickType");
        }
    }
    return bricks;
}
std::string XMLParser::GetBricks()
{
    std::vector<std::vector<std::string>> bricks;

    TiXmlHandle hDoc(file.get());
    TiXmlElement* pRoot, * pParm;
    pRoot = file->FirstChildElement("Level");

    if (pRoot == nullptr)
        return std::string();

    pParm = pRoot->FirstChildElement("Bricks");

    auto text = pParm->GetText();

    return std::string(text == nullptr ? "" : text);

}

std::vector<std::string> XMLParser::GetTags()
{
    return _tags;
}
