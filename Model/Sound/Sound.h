#ifndef SOUND_H
#define SOUND_H

#include <memory>
#include <string>

struct Mix_Chunk;

/*struct _Mix_Music;
typedef struct _Mix_Music Mix_Music;

struct SDL_Mix_MusicDeleter
{
	void operator() (Mix_Music* const p) const noexcept;
}*/;


class Sound
{
public:
	Sound(std::string path);
	~Sound();
	
	Mix_Chunk* GetSound() const;

protected:
	int _type;

	std::unique_ptr<Mix_Chunk> _mix_chunk;
	//std::unique_ptr<Mix_Music, SDL_Mix_MusicDeleter> _mix_music;
};

#endif