#include "SoundManager.h"
#include "../../Configuration.h"
#include <SDL_mixer.h>

#define SOUND_FREQUENCY 22050
#define SOUND_FORMAT AUDIO_S16
#define SOUND_CHANNELS 2
#define SOUND_CHUNKSIZE 4096

SoundManager::SoundManager()
{
	Mix_OpenAudio(SOUND_FREQUENCY, SOUND_FORMAT, SOUND_CHANNELS, SOUND_CHUNKSIZE);
}

SoundManager::~SoundManager()
{
	Mix_CloseAudio();
}

void SoundManager::PlaySound(std::string path)
{
	Mix_PlayChannel(-1, std::make_unique<Sound>(path)->GetSound(), 0);
}

void SoundManager::PlayMusic() const
{
	//Mix_PlayMusic(_music->GetMusic(), -1);
}
