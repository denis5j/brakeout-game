#include "Sound.h"
#include <SDL_mixer.h>

Sound::Sound(std::string path)
{
		_mix_chunk = std::make_unique<Mix_Chunk>(std::move(*Mix_LoadWAV(path.c_str())));
}
Sound::~Sound()
{
}

Mix_Chunk* Sound::GetSound() const
{
	return _mix_chunk.get();
}

//Mix_Music* Sound::GetMusic() const
//{
//	return _mix_music.get();
//}
//
//void SDL_Mix_MusicDeleter::operator()(Mix_Music* const p) const noexcept
//{
//	Mix_FreeMusic(p);
//}
