#ifndef SOUND_MANAGER_H
#define SOUND_MANAGER_H

#include "Sound.h"

class SoundManager
{
public:
	SoundManager();
	~SoundManager();

	static void PlaySound(std::string path);

	void PlayMusic() const;

protected:
};

#endif