#include "Ball.h"
#include "Brick.h"
#include "Player.h"
#include "Wall.h"
#include "../../Configuration.h"

#include "../../View/Event/EventManager.h"
#include "../../View/Event/ChangeStateEvent.h"

#include "../../View/Window.h"


#include <optional>
#include <iostream>

#define PI 3.14159265

Ball::Ball(glm::vec2 position, glm::vec2 size) : Collisionable(position, size), Updatable(), _speed(150.0f), _maxSpeed(900.0f),_increaseFactor(1.05f)
{
	SetImageTexture(Configuration::GetBackButtonPath());

	_direction = glm::normalize(glm::vec2(-0.1f, -1.065f));
}

void Ball::OnUpdate(float deltaTime)
{
	auto findLineIntersection = [this](glm::vec3 line1, glm::vec3 line2)
	{
		glm::vec3 interPointVec3 = glm::cross(line1, line2);

		auto intersectionPoint = glm::vec2(interPointVec3 / interPointVec3.z);

		return intersectionPoint;
	};
	
	auto presjekpravcakruznice = [this](glm::vec2 center, glm::vec2 direction, glm::vec2 edgeVertex, float r)
	{
		float a = glm::length(direction) * glm::length(direction);
		float b = -2.0f * glm::dot((edgeVertex - center), direction);
		float c = glm::length(edgeVertex - center) * glm::length(edgeVertex- center) - r * r;
		float disc = b * b - 4.0f * a * c;

		if (disc >= 0)
		{
			float t1 = (-b + sqrt(disc)) / (2.0f * a);
			float t2 = (-b - sqrt(disc)) / (2.0f * a);
			auto point1 = center + direction * t1;
			auto point2 = center + direction * t2;
			return std::pair<glm::vec2, glm::vec2>(point1, point2);
		}

		return std::pair<glm::vec2, glm::vec2>(glm::vec2(), glm::vec2());
	};

	auto presjekPravcaIElipse = [this](glm::vec2 pointOnLine, glm::vec2 lineDirection, float a, float b)
	{
		float aa = (lineDirection.x * lineDirection.x) / (a * a);
		aa += (lineDirection.y * lineDirection.y) /( b * b);
		float bb = (2.0f * pointOnLine.x * lineDirection.x) / (a * a);
		bb += (2.0f * pointOnLine.y * lineDirection.y) / (b * b);
		float cc = pointOnLine.x * pointOnLine.x / (a * a);
		cc += pointOnLine.y * pointOnLine.y / (b * b);
		cc -= 1.0f;
		float disc = bb * bb - 4.0f * aa * cc;

		if (disc >= 0)
		{
			float t1 = (-bb + sqrt(disc)) / (2.0f * aa);
			float t2 = (-bb - sqrt(disc)) / (2.0f * aa);
			auto point1 = pointOnLine + lineDirection * t1;
			auto point2 = pointOnLine + lineDirection * t2;

			return glm::dot(point1, lineDirection) < glm::dot(pointOnLine, lineDirection) ? point1 : point2;
		}

		return glm::vec2();
	};

	auto findCenterIntersectionPoint = [this, findLineIntersection, presjekpravcakruznice](Edge* closestEdge, Collisionable* colidible)
	{
		std::pair<glm::vec2, glm::vec2> intersectionPointAndNormal;
		
		glm::vec3 ballLine = glm::vec3(glm::vec2(_direction.y, -_direction.x), -glm::dot(glm::vec2(_direction.y, -_direction.x), _centerPosition));
		glm::vec3 closestEdgeLine = closestEdge->GetLine();
		auto edgeBallLineIntersection = findLineIntersection(closestEdgeLine, ballLine);

		auto normal = glm::vec3(glm::vec2(closestEdgeLine.y, -closestEdgeLine.x), -glm::dot(glm::vec2(closestEdgeLine.y, -closestEdgeLine.x), _centerPosition));
		glm::vec3 projectionPointOnEdgeV3 = glm::cross(normal, closestEdgeLine);
		auto projectionPointOnEdge = glm::vec2(projectionPointOnEdgeV3 / projectionPointOnEdgeV3.z);

		//Slicnost trokuta
		auto distanceTilIntersection = glm::length(edgeBallLineIntersection - _centerPosition);
		auto distanceVertical = glm::length(projectionPointOnEdge - _centerPosition);
		float halfSize = _drawingSize.x / 2.0f;
		auto littleDistance = distanceTilIntersection * halfSize / distanceVertical;
		auto centerIntersectionPoint = edgeBallLineIntersection - littleDistance * _direction;

		normal = glm::vec3(glm::vec2(closestEdgeLine.y, -closestEdgeLine.x), -glm::dot(glm::vec2(closestEdgeLine.y, -closestEdgeLine.x), centerIntersectionPoint));
		projectionPointOnEdgeV3 = glm::cross(normal, closestEdgeLine);
		projectionPointOnEdge = glm::vec2(projectionPointOnEdgeV3 / projectionPointOnEdgeV3.z);
		intersectionPointAndNormal.second = projectionPointOnEdge;

		//SpecialCase when is hitting edge
		if (!colidible->IsInrange(projectionPointOnEdge.x, projectionPointOnEdge.y))
		{
			glm::vec2 edgePoint = glm::vec2();
			
			edgePoint = glm::length(closestEdge->GetStartPoint() - centerIntersectionPoint) < glm::length(closestEdge->GetEndPoint() - centerIntersectionPoint) ? closestEdge->GetStartPoint() : closestEdge->GetEndPoint();

			intersectionPointAndNormal.second = edgePoint;

			auto points = presjekpravcakruznice(_centerPosition, _direction, edgePoint, _drawingSize.x / 2.0f);

			if (points.first == glm::vec2())
				return std::pair<glm::vec2,glm::vec2>();

			centerIntersectionPoint = glm::dot(points.first, _direction) < glm::dot(points.second, _direction) ? points.first : points.second;
		}

		intersectionPointAndNormal.first = centerIntersectionPoint;

		return intersectionPointAndNormal;
	};

	auto findClosestEdge = [this, findCenterIntersectionPoint](Collisionable* colidible)
	{
		Edge* closestEdge = nullptr;
		double min = FLT_MAX;

		for (auto &edge : colidible->GetEdge())
		{
			auto intersectionPoint = findCenterIntersectionPoint(edge, colidible).first;
			double distance = glm::distance(intersectionPoint, _centerPosition);
			
			if (distance <= min)
			{
				closestEdge = edge;
				min = distance;
			}
		}

		return closestEdge;
	};

	auto CalculateTimeTillCollision = [this,findCenterIntersectionPoint,findClosestEdge](float &minTime, Collisionable* &closestColidible, std::pair<glm::vec2, glm::vec2> &pointandnormal)
	{
		for (auto colidible : _activeCollisionables)
		{
			if (colidible == dynamic_cast<Ball*>(this))
				continue;

			auto isCollisionPossible = [this](Collisionable* collisionable)
			{
				auto centardot = glm::dot(_centerPosition, _direction);

				for (auto edge : collisionable->GetEdge())
				{
					if (centardot < glm::dot(edge->GetStartPoint(), _direction))
						return true;
				}

				return false;
			};

			if (!isCollisionPossible(colidible))
				continue;

			Edge* closestEdge = findClosestEdge(colidible);

			if (closestEdge == nullptr)
				continue;

			auto pointnormal = findCenterIntersectionPoint(closestEdge, colidible);

			auto intersectionPointCenter = pointnormal.first;

			float distanceTilCrash = glm::length(intersectionPointCenter - _centerPosition);

			float timeTilCrash = distanceTilCrash / _speed;

			if (timeTilCrash < minTime)
			{
				minTime = timeTilCrash;

				closestColidible = colidible;

				pointandnormal = pointnormal;
			}
		}

	};

	float minTimeTillCrash = FLT_MAX;
	Collisionable* closestColidible = nullptr;
	std::pair<glm::vec2, glm::vec2> pointandnormal;

	CalculateTimeTillCollision(minTimeTillCrash, closestColidible, pointandnormal);

	do {

		if (minTimeTillCrash < deltaTime)
		{
			auto deltaPosition = minTimeTillCrash * _direction * _speed;

			_drawingPosition += deltaPosition;
			_centerPosition += deltaPosition;

			deltaTime -= minTimeTillCrash;

			auto edges = closestColidible->GetEdges();

			Edge* closestEdge = findClosestEdge(closestColidible);

			auto normal = pointandnormal.second - pointandnormal.first;
			normal = glm::vec2(-normal.y, normal.x);

			if (auto player = dynamic_cast<Player*>(closestColidible))
			{
				auto playerHalfSize = player->GetSize() / 2.0f;
				auto playerTopCenter = player->GetCenter() - glm::vec2(0.0f, playerHalfSize.y);

				auto intersectionPoint = pointandnormal.first;
				intersectionPoint -= playerTopCenter;

				auto point = presjekPravcaIElipse(intersectionPoint, _direction, playerHalfSize.x, playerHalfSize.y);

				if (point != glm::vec2())
				{
					point += playerTopCenter;

					normal = glm::vec2(2 * (point.x - playerTopCenter.x) / (playerHalfSize.x * playerHalfSize.x), 2 * (point.y - playerTopCenter.y) / (playerHalfSize.y * playerHalfSize.y));

					normal = glm::vec2(-normal.y, normal.x);
				}
			}

			float angle = atan2(_direction.y, _direction.x) - atan2(normal.y, normal.x);

			glm::mat2x2 rotationMatrix = { cos(-2 * angle),-sin(-2 * angle),sin(-2 * angle),cos(-2 * angle) };

			_direction = _direction * rotationMatrix;

			if (auto brick = dynamic_cast<Brick*>(closestColidible))
				brick->Hit();

			_speed *= _increaseFactor;

			if (_speed > _maxSpeed)
				_speed = _maxSpeed;
		}

		minTimeTillCrash = FLT_MAX;

		CalculateTimeTillCollision(minTimeTillCrash, closestColidible, pointandnormal);

	} while (minTimeTillCrash < deltaTime);



	auto deltaPosition = deltaTime * _direction * _speed;

	_drawingPosition += deltaPosition;
	_centerPosition += deltaPosition;

	if (_drawingPosition.y > Window::GetHeight())
	{
		EventManager::AddEvent(std::make_unique<ChangeStateEvent>(1));
		exit(0);
	}
}

std::vector<glm::vec3> Ball::GetEdges()
{
	return std::vector<glm::vec3>();
}
