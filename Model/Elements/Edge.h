#ifndef EDGE_H
#define EDGE_H

#include <glm/vec2.hpp>
#include <glm/mat2x2.hpp>
#include <glm/gtx/string_cast.hpp>

class Edge
{
public:
	Edge(glm::vec2 startPoint, glm::vec2 endPoint);

	glm::vec2 GetStartPoint();
	glm::vec2 GetEndPoint();
	glm::vec3 GetLine();

protected:
	glm::vec2 _startPoint;
	glm::vec2 _endPoint;
	glm::vec3 _line;
};

#endif