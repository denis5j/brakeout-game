#include "Button.h"

Button::Button(glm::vec2 position, glm::vec2 size, std::string path, bool enabled) : Drawable(position, size), _enabled(enabled)
{
	SetImageTexture(path);
}

void Button::SetEnabled(bool enabled)
{
	_enabled = enabled;
}

bool Button::IsEnabled() const
{
	return _enabled;
}
