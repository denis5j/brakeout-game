#include "Background.h"
#include "../../Configuration.h"

Background::Background(glm::vec2 position, glm::vec2 size, std::string path) : Drawable(position, size)
{
	SetImageTexture(path);
}
