#ifndef BALL_H
#define BALL_H

#include "../Updatable.h"
#include "../Collisionable.h"

class Ball : public Collisionable, public Updatable
{
public:
	Ball(glm::vec2 position, glm::vec2 size);

	void OnUpdate(float deltaTime);

	std::vector<glm::vec3> GetEdges(); 

protected:
	float _speed;
	float _maxSpeed;
	float _increaseFactor;
	
	glm::vec2 _direction;

};

#endif