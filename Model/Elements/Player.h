#ifndef PLAYER_H
#define PLAYER_H

#include <string>
#include "../Collisionable.h"

class Player : public Collisionable
{
public:
	Player(glm::vec2 position, glm::vec2 size);

	void GivePoints(int points);
	int GetPoints() const;

	std::string GetName() const;

	std::vector<glm::vec3> GetEdges();

	void ChangePosition(glm::vec2 position);

	void ReduceLife();
	int GetLife() const;

protected:
	std::string _name;

	int _points;
	int _life;

	std::vector<glm::vec3> _edges;
};

#endif