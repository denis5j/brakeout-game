#include "Player.h"
#include "../../Configuration.h"

Player::Player(glm::vec2 position, glm::vec2 size) : Collisionable(position, size), _points(0), _life(3)
{
	_name = "Player 1";
	SetImageTexture(Configuration::GetCardBackPath());

	auto direction = glm::vec2(0.0f, -1.0f);
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position + size)));
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position)));

	direction = glm::vec2(-1.0f, 0.0f);
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position + size)));
	_edges.push_back(glm::vec3(direction, -glm::dot(direction, position)));
} 

void Player::GivePoints(int points)
{
	_points += points;
}

int Player::GetPoints() const
{
	return _points;
}

std::string Player::GetName() const
{
	return _name;
}

std::vector<glm::vec3> Player::GetEdges()
{
	return _edges;
}

void Player::ChangePosition(glm::vec2 position)
{
	SetPosition(position);

	auto direction = glm::vec2(-1.0f, 0.0f);
	_edges[2] = glm::vec3(direction, -glm::dot(direction, position + _drawingSize));
	_edges[3] = glm::vec3(direction, -glm::dot(direction, position));

	UpdateEdges(position, _drawingSize);
}

void Player::ReduceLife()
{
	_life--;
}

int Player::GetLife() const
{
	return _life;
}
