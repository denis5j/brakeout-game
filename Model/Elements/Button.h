#ifndef BUTTON_H
#define BUTTON_H

#include "../Drawable.h"

class Button : public Drawable
{
public:
	Button(glm::vec2 position, glm::vec2 size, std::string path, bool enabled = true);

	void SetEnabled(bool enabled);
	bool IsEnabled() const;

protected:
	bool _enabled;
};

#endif