#ifndef TEXT_H
#define TEXT_H

#include "../Drawable.h"

class Text : public Drawable
{
public:
	Text(glm::vec2 position, std::string text, int fontSize, double angle = 0.0, bool centered = true);
	
	void SetText(std::string text,int fontSize = -1);
	void SetColor(int r, int g, int b);

protected:
	bool _centered;
	
	int _fontSize;
	int _r, _g, _b;
	
	glm::vec2 _position;
};

#endif