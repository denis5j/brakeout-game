#include "Edge.h"

Edge::Edge(glm::vec2 startPoint, glm::vec2 endPoint) : _startPoint(startPoint), _endPoint(endPoint)
{
	auto direction = glm::normalize(startPoint - endPoint);
	_line = glm::vec3(direction, -glm::dot(direction, startPoint));
}

glm::vec2 Edge::GetStartPoint()
{
	return _startPoint;
}

glm::vec2 Edge::GetEndPoint()
{
	return _endPoint;
}

glm::vec3 Edge::GetLine()
{
	return _line;
}
