#include "Model.h"
#include "../Configuration.h"
#include "../View/Window.h"
#include <algorithm>

Model::Model()
{
	_xmlParser = std::make_shared<XMLParser>();
	_xmlParser->LoadFile("Assets/Bricks.xml");
	_currentState = std::make_unique<MainMenu>();
	_background = std::make_unique<Background>(glm::vec2(), glm::vec2(Window::GetWidth(), Window::GetHeight()), _xmlParser->GetLevelData("BackgroundTexture"));
}

void Model::CreatePlayer(std::string name)
{
	_players.push_back(std::make_unique<Player>(glm::vec2(600 - 100, 900), glm::vec2(200, 20)));
}

void Model::DeletePlayers()
{
	_players.clear();
}

std::vector<Player*> Model::GetPlayers() const
{
	std::vector<Player*> players;

	for (auto& player : _players)
		players.push_back(player.get());

	return players;
}

void Model::CreateMainMenu()
{
	_currentState = std::make_unique<MainMenu>();
}

MainMenu* Model::GetMainMenu() const
{
	return std::get<std::unique_ptr<MainMenu>>(_currentState).get();
}

void Model::CreateVictory()
{
	std::vector<Player*> players;

	for (auto &player : _players)
		players.push_back(player.get());

	_currentState = std::make_unique<VictoryMenu>(players);
}

VictoryMenu* Model::GetVictory() const
{
	return std::get<std::unique_ptr<VictoryMenu>>(_currentState).get();
}

void Model::CreateGameScene()
{
	std::vector<Player*> players;

	for (auto& player : _players)
		players.push_back(player.get());

	_currentState = std::make_unique<GameScene>(players, _xmlParser);
}

GameScene* Model::GetGameScene() const
{
	return std::get<std::unique_ptr<GameScene>>(_currentState).get();
}
