#include "Drawable.h"
#include "../Model/Texture/TextureManager.h"

#define PI 3.14159265

std::vector<Drawable const*> Drawable::_activeDrawables;

Drawable::Drawable(glm::vec2 position, glm::vec2 size, double angle) : _drawingPosition(position), _drawingSize(size), _drawingRotation(size.x / 2.0f, size.y / 2.0f), _angle(angle)
{
	_centerPosition = position + size / 2.0f;
	_texture = std::make_unique<Texture>();
	_activeDrawables.push_back(this);
}

Drawable::~Drawable()
{
	for (int i = 0; i < _activeDrawables.size(); i++)
	{
		if (_activeDrawables[i] == this)
		{
			_activeDrawables.erase(_activeDrawables.begin() + i);
			break;
		}
	}
}

SDL_Texture* Drawable::GetTexture() const
{
	return _texture->GetTexture() == nullptr ? TextureManager::GetTexture(_path)->GetTexture() : _texture->GetTexture();
}

void Drawable::SetImageTexture(std::string path)
{
	_path = path;
	TextureManager::GetTexture(_path);
}

void Drawable::SetTextTexture(std::string text, int fontSize, int r, int g, int b)
{
	auto size = _texture->CreateTextTexture(text, fontSize, r, g, b);

	_drawingSize.x =  size.first;
	_drawingSize.y = size.second;
}

bool Drawable::IsInrange(float x, float y) const
{
	if (_drawingPosition.x <= x && x <= _drawingPosition.x + _drawingSize.x &&
		_drawingPosition.y <= y && y <= _drawingPosition.y + _drawingSize.y)
		return true;

	return false;
}

void Drawable::SetPosition(glm::vec2 position)
{
	_drawingPosition = position;
	_centerPosition = position + _drawingSize / 2.0f;
}

glm::vec2 Drawable::GetPosition() const
{
	return _drawingPosition;
}

void Drawable::SetSize(glm::vec2& size)
{
	_drawingSize = size;
}

glm::vec2 Drawable::GetSize() const
{
	return _drawingSize;
}

void Drawable::SetRotation(glm::vec2& rotation)
{
	_drawingRotation = rotation;
}

glm::vec2 Drawable::GetRotation() const
{
	return _drawingRotation;
}

void Drawable::SetAngle(double angle, int x, int y)
{
	_drawingRotation.x = x;
	_drawingRotation.y = y;
	_angle = angle;
}

double Drawable::GetAngle() const
{
	return _angle;
}

glm::vec2 Drawable::GetCenter()
{
	return _centerPosition;
}
