#ifndef XMLPARSER_H
#define XMLPARSER_H

#include <memory>
#include <map>
#include <vector>
#include <string>

class TiXmlDocument;

class XMLParser
{
public:
	XMLParser();
	~XMLParser();

	void LoadFile(std::string path);
	std::string GetLevelData(std::string dataName);
	std::map<std::string,std::map<std::string,std::string>> GetBrickTypes();
	std::string GetBricks();
	std::vector<std::string> GetTags();


protected:
	std::vector<std::string> _tags;
	std::unique_ptr<TiXmlDocument> file;
};

#endif