#ifndef MODEL_H
#define MODEL_H

#include <string>
#include "Elements/Player.h"
#include "Elements/Text.h"
#include "Elements/Background.h"
#include "Scenes/MainMenu.h"
#include "Scenes/VictoryMenu.h"
#include "Scenes/GameScene.h"
#include "XMLParser.h"
#include "../Configuration.h"

#include <variant>

class Model
{
public:
	Model();

	void CreatePlayer(std::string name);
	void DeletePlayers();
	std::vector<Player*> GetPlayers() const;

	void CreateMainMenu();
	MainMenu* GetMainMenu() const;
	void CreateVictory();
	VictoryMenu* GetVictory() const;
	void CreateGameScene();
	GameScene* GetGameScene() const;

protected:
	std::shared_ptr<XMLParser> _xmlParser;
	std::unique_ptr<Background> _background;
	std::unique_ptr<Configuration> _configuration;

	std::variant<std::unique_ptr<MainMenu>, std::unique_ptr<GameScene>, std::unique_ptr<VictoryMenu>> _currentState;

	std::vector<std::unique_ptr<Player>> _players;
};

#endif