#ifndef TEXTURE_H
#define TEXTURE_H

#include <memory>
#include <string>

struct SDL_Texture;

struct SDL_TextureDeleter
{
	void operator() (SDL_Texture* const p) const noexcept;
};

class Texture
{
public:
	Texture();
	~Texture();

	SDL_Texture* GetTexture() const;

	std::pair<int,int> CreateTextTexture(std::string text, int fontSize, int r, int g, int b);
	void CreateImageTexture(std::string path);
	
protected:
	SDL_Texture* LoadTexture(std::string &path);

	std::unique_ptr<SDL_Texture, SDL_TextureDeleter> _texture;
};

#endif