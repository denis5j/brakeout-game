#ifndef TEXTURE_MANAGER_H
#define TEXTURE_MANAGER_H

#include "Texture.h"
#include <map>

class TextureManager
{
public:
	TextureManager();
	~TextureManager();

	static Texture* GetTexture(const std::string& path, bool image = true);

protected:

	static void LoadImageTexture(const std::string& path);

	static std::map<std::string, std::unique_ptr<Texture>> _mapImages;
};

#endif