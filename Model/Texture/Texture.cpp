#include "Texture.h"
#include "../../View/Window.h"
#include "../../Configuration.h"
#include <SDL.h>
#include <SDL_image.h>
#include <SDL_ttf.h>
#include <SDL_render.h>

Texture::Texture()
{
}

Texture::~Texture()
{
}

SDL_Texture* Texture::LoadTexture(std::string &path)
{
    SDL_Texture* newTexture = nullptr;

    //Load image at specified path
    SDL_Surface* loadedSurface = IMG_Load(path.c_str());

    if (loadedSurface == NULL)
    {
        printf("Unable to load image %s! SDL_image Error: %s\n", path.c_str(), IMG_GetError());
    }
    else
    {
        //Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface(Window::GetRenderer(), loadedSurface);
        if (newTexture == NULL)
        {
            printf("Unable to create texture from %s! SDL Error: %s\n", path.c_str(), SDL_GetError());
        }

        //Get rid of old loaded surface
        SDL_FreeSurface(loadedSurface);
    }

    return newTexture;
}


SDL_Texture* Texture::GetTexture() const
{
    return _texture.get();
}

std::pair<int, int> Texture::CreateTextTexture(std::string text, int fontSize, int r, int g, int b)
{
    TTF_Init();

    SDL_Texture* newTexture = nullptr;

    SDL_Color textColor = { (Uint8)r, (Uint8)g, (Uint8)b };

    TTF_Font* gFont = TTF_OpenFont(Configuration::GetFontPath().c_str(), fontSize);

    SDL_Surface* textSurface = TTF_RenderText_Solid(gFont, text.c_str(), textColor);

    std::pair<int, int> size;

    if (gFont == NULL)
    {
        printf("Failed to load lazy font! SDL_ttf Error: %s\n", TTF_GetError());
    }
    else
    {
        //Create texture from surface pixels
        newTexture = SDL_CreateTextureFromSurface(Window::GetRenderer(), textSurface);
        if (newTexture == NULL)
        {
            printf("Unable to create texture from %s! SDL Error: %s\n", text.c_str(), SDL_GetError());
        }

        size.first = textSurface->w;
        size.second = textSurface->h;

        //Get rid of old loaded surface
        SDL_FreeSurface(textSurface);
    }

    TTF_CloseFont(gFont);

    _texture = std::unique_ptr<SDL_Texture, SDL_TextureDeleter>(newTexture);

    return size;
}

void Texture::CreateImageTexture(std::string path)
{
    _texture = std::unique_ptr<SDL_Texture, SDL_TextureDeleter>(LoadTexture(path));
}

void SDL_TextureDeleter::operator()(SDL_Texture* const p) const noexcept
{
    SDL_DestroyTexture(p);
}
