#include "Collisionable.h"

std::vector<Collisionable*> Collisionable::_activeCollisionables;

Collisionable::Collisionable(glm::vec2 position, glm::vec2 size) : Drawable(position, size)
{
	_activeCollisionables.push_back(this);
	UpdateEdges(position, size);
}

Collisionable::~Collisionable()
{
	for (int i = 0; i < _activeCollisionables.size(); i++)
	{
		if (this == _activeCollisionables[i]) {
			_activeCollisionables.erase(_activeCollisionables.begin() + i);
			break;
		}
	}
}

std::vector<Edge*> Collisionable::GetEdge()
{
	std::vector<Edge*> edges;

	for (auto& edge : _edge)
		edges.push_back(edge.get());

	return edges;
}

void Collisionable::UpdateEdges(glm::vec2 position, glm::vec2 size)
{
	_edge.clear();

	auto tl = position;
	auto bl = position + glm::vec2(0.0f, size.y);
	auto tr = position + glm::vec2(size.x, 0.0f);
	auto br = position + size;

	_edge.push_back(std::make_unique<Edge>(tl, tr));
	_edge.push_back(std::make_unique<Edge>(tr, br));
	_edge.push_back(std::make_unique<Edge>(br, bl));
	_edge.push_back(std::make_unique<Edge>(bl, tl));
}
