#ifndef BRICK_DESTROYED_EVENT_H
#define BRICK_DESTROYED_EVENT_H

#include "Event.h"

class BrickDestroyedEvent : public Event
{
public:
	BrickDestroyedEvent();

protected:
};

#endif