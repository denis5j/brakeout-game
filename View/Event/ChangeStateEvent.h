#ifndef CHANGE_STATE_EVENT_H
#define CHANGE_STATE_EVENT_H

#include "Event.h"

class ChangeStateEvent : public Event
{
public:
	ChangeStateEvent(int state);

	const int GetState() const;

protected:
	const int _state;
};

#endif