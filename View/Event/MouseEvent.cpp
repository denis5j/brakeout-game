#include "MouseEvent.h"

MouseEvent::MouseEvent(int x, int y, int button, int state) : Event(), _x(x), _y(y), _button(button), _state(state)
{
}

int MouseEvent::GetX() const
{
	return _x;
}

int MouseEvent::GetY() const
{
	return _y;
}

int MouseEvent::GetButton() const
{
	return _button;
}

int MouseEvent::GetState() const
{
	return _state;
}

