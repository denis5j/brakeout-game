#ifndef EVENT_MANAGER_H
#define EVENT_MANAGER_H

#include <vector>
#include <memory>

#include "Event.h"

union SDL_Event;

class EventManager
{
public:
	EventManager();

	std::vector<std::unique_ptr<Event>> GetEvents() const;

	void OnKeyboardLeftButton(SDL_Event const& event) const;
	void OnKeyboardRightButton(SDL_Event const& event) const;

	void OnMouseButtonDown(SDL_Event const& event) const;
	void OnMouseButtonUp(SDL_Event const& event) const;
	void OnMouseMotion(SDL_Event const& event) const;
	static void AddEvent(std::unique_ptr<Event> event);

protected:
	static std::vector<std::unique_ptr<Event>> _events;
};

#endif