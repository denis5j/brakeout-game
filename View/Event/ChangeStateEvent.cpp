#include "ChangeStateEvent.h"

ChangeStateEvent::ChangeStateEvent(int state) : Event(), _state(state)
{
}

const int ChangeStateEvent::GetState() const
{
	return _state;
}
