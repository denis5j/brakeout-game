#ifndef MOUSE_EVENT_H
#define MOUSE_EVENT_H

#include "Event.h"

class MouseEvent : public Event
{
public:
	MouseEvent(int x, int y, int button, int state);

	int GetX() const;
	int GetY() const;
	int GetButton() const;
	int GetState() const;

protected:
	int _x;
	int _y;
	int _button;
	int _state;
};

#endif