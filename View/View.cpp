#include "View.h"

View::View(Window* window, Model* model)
{
	_window = window;
	_model = model;
	_renderer = std::make_unique<Renderer>();
	_eventManager = std::make_unique<EventManager>();
}

void View::Render() const
{
	_renderer->ClearRenderer();

	for (auto const &drawable : Drawable::_activeDrawables)
		_renderer->Render(drawable);

	Window::UpdateWindow();
}

std::vector<std::unique_ptr<Event>> View::GetEvents() const
{
	return _eventManager->GetEvents();
}
