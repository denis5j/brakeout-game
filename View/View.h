#ifndef VIEW_H
#define VIEW_H

#include <string>
#include <vector>
#include <memory>
#include "Window.h"
#include "Event/Event.h"
#include "Event/EventManager.h"
#include "../Model/Model.h"
#include "Renderer.h"

class View
{
public:
	View(Window* window, Model* model);

	void Render() const;

	std::vector<std::unique_ptr<Event>> GetEvents() const;

protected:
	Window* _window;
	Model* _model;

	std::unique_ptr<Renderer> _renderer;
	std::unique_ptr<EventManager> _eventManager;
};

#endif