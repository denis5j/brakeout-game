#include "Window.h"

#include <SDL_image.h>

std::unique_ptr<SDL_Renderer, SDL_Renderer_Deleter> Window::_renderer;
int Window::_windowWidth;
int Window::_windowHeight;

Window::Window(std::string title, int width, int height) : _window(nullptr), _windowTitle(title)
{
    _windowWidth = width;
    _windowHeight = height;

	_window = std::unique_ptr<SDL_Window, SDL_Window_Deleter>(SDL_CreateWindow(_windowTitle.c_str(), SDL_WINDOWPOS_CENTERED, SDL_WINDOWPOS_CENTERED,
        _windowWidth, _windowHeight, SDL_WINDOW_SHOWN));
    
    _renderer = std::unique_ptr<SDL_Renderer, SDL_Renderer_Deleter>(SDL_CreateRenderer(_window.get(), -1, SDL_RENDERER_ACCELERATED));

    SDL_SetRenderDrawColor(_renderer.get(), 0xFF, 0xFF, 0xFF, 0xFF);

    IMG_Init(IMG_INIT_JPG);

    _surface = SDL_GetWindowSurface(_window.get());

    SDL_SetRenderDrawColor(_renderer.get(), 0x00, 0x00, 0x00, 0xFF);
}

SDL_Renderer* Window::GetRenderer()
{
    return _renderer.get();
}

int Window::GetWidth()
{
    return _windowWidth;
}

int Window::GetHeight()
{
    return _windowHeight;
}

void Window::ClearRenderer()
{
    SDL_RenderClear(_renderer.get());
}

void Window::UpdateWindow()
{
    SDL_RenderPresent(_renderer.get());
}

void SDL_Renderer_Deleter::operator()(SDL_Renderer* const p) const noexcept
{
    SDL_DestroyRenderer(p);
}

void SDL_Window_Deleter::operator()(SDL_Window* const p) const noexcept
{
    SDL_DestroyWindow(p);
}
