#ifndef TIMER_H
#define TIMER_H

#include <stdint.h>

class Timer
{
public:
	Timer();
	~Timer();

	int GetMilisecondsDelta() const;

	void Reset();
protected:
	int m_milliSecondTicks;
};

#endif