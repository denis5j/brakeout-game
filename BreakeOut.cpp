#include <SDL.h>
#include <memory>
#include <time.h>

#include "View/Window.h"
#include "Model/Model.h"
#include "View/View.h"
#include "Controller/Controller.h"
#include "Timer.h"

#define FPS 60.0
#define FPSMILISECONDS 1000.0/FPS

std::unique_ptr<Window> _window;
std::unique_ptr<Model> _model;
std::unique_ptr<View> _view;
std::unique_ptr<Controller> _controller;
std::unique_ptr<Timer> _timer;

int main(int argc, char* args[])
{
	srand(time(NULL));

	_window = std::make_unique<Window>("Brakeout game", 1200, 1000);
	
	_model = std::make_unique<Model>();
	_view = std::make_unique<View>(_window.get(), _model.get());
	_controller = std::make_unique<Controller>(_model.get(), _view.get());
	_timer = std::make_unique<Timer>();

	_timer->Reset();

	while (true)
	{
		auto time = _timer->GetMilisecondsDelta();
		_timer->Reset();

		_controller->OnUpdate(time/1000.0f);
		_view->Render();

		while (_timer->GetMilisecondsDelta() < FPSMILISECONDS);
	}

	return 0;
}