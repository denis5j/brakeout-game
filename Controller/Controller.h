#ifndef CONTROLLER_H
#define CONTROLLER_H

#include <string>

#include "../Model/Model.h"
#include "../View/View.h"
#include "../View/Event/Event.h"
#include "../Model/Sound/SoundManager.h"
#include "States/State.h"
  
class Controller
{
public:
	Controller(Model* model, View* view);

	void OnUpdate(float deltaTime);
	void ProcessEvent(const Event& event);

protected:
	Model* _model;
	View* _view;

	std::unique_ptr<State> _currentState;

	std::unique_ptr<SoundManager> _soundManager;

	enum states {gameState,victoryState,mainMenuState};
};

#endif