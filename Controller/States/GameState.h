#ifndef GAMESTATE_H
#define GAMESTATE_H

#include "State.h"
#include "../../Model/Updatable.h"
#include "../../View/Event/Event.h"
#include "../../Model/Model.h"
#include "../../Model/Sound/SoundManager.h"


class GameState : public State
{
public:
	GameState(Model* model, SoundManager* soundManager);
	~GameState();

	void ProcessEvent(const Event& event) override;
	//void OnUpdate(const Event& event) override;

protected:
	Model* _model;
	SoundManager* _soundManager;
};

#endif