#ifndef STATE_H
#define STATE_H

#include "../../View/Event/Event.h"

class State
{
public:
	virtual ~State();

	virtual void ProcessEvent(const Event& event) = 0;
};

#endif