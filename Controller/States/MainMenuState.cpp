#include "MainMenuState.h"
#include "../../View/Event/MouseEvent.h"
#include "../../View/Event/EventManager.h"
#include "../../View/Event/ChangeStateEvent.h"
#include "GameState.h"

#define MINCARDS 2 

MainMenuState::MainMenuState(Model* model, SoundManager* soundManager) : _model(model), _soundManager(soundManager)
{
	_model->CreateMainMenu();
}

void MainMenuState::ProcessEvent(const Event& event)
{
	if (const MouseEvent* mouseEvent = dynamic_cast<const MouseEvent*>(&event))
	{
		if (!mouseEvent->GetState())
			return;

		auto numberOfPlayerButtons = _model->GetMainMenu()->GetNumberOfPlayersButtons();

		auto gui = _model->GetMainMenu();

		auto enableDisableButtons = [](std::pair<Button*, Button*> firstButtons, std::pair<Button*,Button*> secondButtons, int &guiNumber, int increment)
		{
			if ((guiNumber += increment) > MINCARDS)
				firstButtons.first->SetEnabled(true);
			else
				firstButtons.first->SetEnabled(false);

			bool isEven = guiNumber % 2 == 0;

			secondButtons.first->SetEnabled(isEven);
			secondButtons.second->SetEnabled(isEven);
		};

		auto widthButtons = _model->GetMainMenu()->GetWidthButtons();
		auto heightButtons = _model->GetMainMenu()->GetHightButtons();

		if (gui->_cardArrayWidth == MINCARDS)
			widthButtons.first->SetEnabled(false);

		if (gui->_cardArrayHeight == MINCARDS)
			heightButtons.first->SetEnabled(false);
		
		if (numberOfPlayerButtons.first->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()) && numberOfPlayerButtons.first->IsEnabled())
			numberOfPlayerButtons.first->SetEnabled(--gui->_numberOfPlayers > 1);
		else if (numberOfPlayerButtons.second->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()) && numberOfPlayerButtons.second->IsEnabled())
			numberOfPlayerButtons.first->SetEnabled(++gui->_numberOfPlayers > 1);
		else if (widthButtons.first->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()) && widthButtons.first->IsEnabled())
			enableDisableButtons(widthButtons, heightButtons, gui->_cardArrayWidth, -1);
		else if (widthButtons.second->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()) && widthButtons.second->IsEnabled())
			enableDisableButtons(widthButtons, heightButtons, gui->_cardArrayWidth, 1);
		else if (heightButtons.first->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()) && heightButtons.first->IsEnabled())
			enableDisableButtons(heightButtons, widthButtons, gui->_cardArrayHeight, -1);
		else if (heightButtons.second->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()) && heightButtons.second->IsEnabled())
			enableDisableButtons(heightButtons, widthButtons, gui->_cardArrayHeight, 1);

		auto startGameButton = gui->GetStartButton();

		if (startGameButton->IsInrange(mouseEvent->GetX(), mouseEvent->GetY()))
			EventManager::AddEvent(std::make_unique<ChangeStateEvent>(0));
	}
}
